functor SortList (Elem : ORD_KEY) :> sig
  val qsort : Elem.ord_key list -> Elem.ord_key list
  val msort : Elem.ord_key list -> Elem.ord_key list
end = struct

  fun splitOn pivot (x, (smaller, larger)) =
      case Elem.compare (x, pivot)
        of (LESS | EQUAL) => (x :: smaller, larger)
         | GREATER        => (smaller, x :: larger)

  fun qsort [] = []
    | qsort (x :: xs) = let
      val (smaller, larger) = foldl (splitOn x) ([], []) xs
    in 
      qsort smaller @ [x] @ qsort larger
    end

  fun merge xs [] = xs
    | merge [] ys = ys
    | merge (left as x :: xs) (right as y :: ys) =
      case Elem.compare (x, y)
        of (LESS | EQUAL) => x :: merge xs right
         | GREATER        => y :: merge left ys

  fun msort [] = []
    | msort [x] = [x]
    | msort xs = let
      val m = length xs div 2
      val (left, right) = (List.take (xs, m), List.drop (xs, m))
    in
      merge (msort left) (msort right)
    end

end

