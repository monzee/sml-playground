functor Compr (M : sig
  type 'a t
  val empty : 'a t
  val bnd : 'a t * ('a -> 'b t) -> 'b t
  val ret : 'a -> 'a t
end) = struct
  val >>= = M.bnd
  fun op>> (_, xs) = xs
  val return = M.ret
  fun when false _ = M.empty
    | when true thunk = thunk ()
  fun var x f = f x
end

structure ListComp = Compr (struct
  type 'a t = 'a list
  val empty = []
  fun bnd (xs, f) = List.concat (map f xs)
  fun ret a = [a]
end)

open ListComp
infixr 1 >>= 
infixr 1 >>

val c = ref 0;

  [0, 1, 2] >>= (fn x =>
  [4, 5, 6] >>= (fn y =>
  c := !c + 1 >>
  when (x + y = 6) (fn _ =>
  var (Int.toString x, y * 2) (fn (x, y') =>
  print (Int.toString (!c) ^ "\n") >>
  ["a", "b", "c"] >>= (fn z =>
  return (x ^ z, y, y'))))))

