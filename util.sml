signature COMPARATOR = sig
  type ord_key
  val compare : ord_key * ord_key -> order
  val =  : ord_key * ord_key -> bool
  val <> : ord_key * ord_key -> bool
  val >  : ord_key * ord_key -> bool
  val >= : ord_key * ord_key -> bool
  val <  : ord_key * ord_key -> bool
  val <= : ord_key * ord_key -> bool
end

structure Util : sig
  datatype 'a control = next of 'a | break of 'a

  structure Comp : sig
    functor MakeComparator (C : ORD_KEY) : COMPARATOR
    structure Int : COMPARATOR
    structure Real : COMPARATOR
    structure String : COMPARATOR
    structure StringI : COMPARATOR
  end

  structure Control : sig
    val map : ('a -> 'b control) -> 'a list -> 'b list
    val foldl : ('a * 'b -> 'b control) -> 'b -> 'a list -> 'b
  end

  val id : 'a -> 'a
  val pairMap : ('a -> 'b) -> 'a * 'a -> 'b * 'b
  val flip : ('a -> 'b -> 'c) -> 'b -> 'a -> 'c
  val uncurry2 : ('a -> 'b -> 'c) -> 'a * 'b -> 'c
  val uncurry3 : ('a -> 'b -> 'c -> 'd) -> 'a * 'b * 'c -> 'd
  val uncurry4 : ('a -> 'b -> 'c -> 'd -> 'e) -> 'a * 'b * 'c * 'd -> 'e
  val untuple2 : ('a * 'b -> 'c) -> 'a -> 'b -> 'c
  val untuple3 : ('a * 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd
  val untuple4 : ('a * 'b * 'c * 'd -> 'e) -> 'a -> 'b -> 'c -> 'd -> 'e
  val $ : ('a -> 'b) * 'a -> 'b
  val |> : 'a * ('a -> 'b) -> 'b
end = struct
  datatype 'a control = next of 'a | break of 'a

  fun id x = x
  fun flip f x y = f y x
  fun uncurry2 f (a, b) = f a b
  fun uncurry3 f (a, b, c) = f a b c
  fun uncurry4 f (a, b, c, d) = f a b c d
  fun untuple2 f a b = f (a, b)
  fun untuple3 f a b c = f (a, b, c)
  fun untuple4 f a b c d = f (a, b, c, d)
  fun op $ (f, x) = f x
  fun op |> (x, f) = f x
  fun pairMap f (a, b) = (f a, f b)

  structure Comp = struct
    functor MakeComparator (C : ORD_KEY) : COMPARATOR = struct
      open C
      fun op = p = case compare p
          of EQUAL => true
           | _ => false
      fun op <> p = case compare p
          of (LESS | GREATER) => true
           | _ => false
      fun op > p = case compare p
          of GREATER => true
           | _ => false
      fun op >= p = case compare p
          of (GREATER | EQUAL) => true
           | _ => false
      fun op < p = case compare p
          of LESS => true
           | _ => false
      fun op <= p = case compare p
          of (LESS | EQUAL) => true
           | _ => false
    end

    structure Int = MakeComparator (struct
      type ord_key = int
      val compare = Int.compare
    end)

    structure Real = MakeComparator (struct
      type ord_key = real
      val compare = Real.compare
    end)

    structure StringI = MakeComparator (struct
      type ord_key = string
      val compare = String.collate (Char.compare o (pairMap Char.toLower))
    end)

    structure String = MakeComparator (struct
      type ord_key = string
      val compare = String.compare
    end)
  end
  
  structure Control = struct
    fun map _ [] = [] 
      | map f (x :: xs) = case f x 
        of break y => [y] 
         | next y => y :: map f xs

    fun foldl _ acc [] = acc 
      | foldl f acc (x :: xs) = case f (x, acc) 
        of break y => y 
         | next y => foldl f y xs
  end
end
