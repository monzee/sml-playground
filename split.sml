structure Split :> sig
  type t
  val source : string -> t
  val quoted : string * char -> t
  val enclosed : string * string -> t
  val splitAt : t -> char -> string list
end = struct
  datatype delim = Quote of char | Braces of char * char
  datatype t = Plain of string | Delimited of string * delim
  exception Unimplemented

  fun source s = Plain s
  fun quoted (s, q) = Delimited (s, Quote q)
  fun enclosed (s, delims) = 
      case explode delims
        of left :: right :: _ => Delimited (s, Braces (left, right))
         | [c] => Delimited (s, Quote c)
         | _ => Plain s


  fun splitAt (Plain s) sep = raise Unimplemented
    | splitAt (Delimited (s, d)) sep = raise Unimplemented
end

