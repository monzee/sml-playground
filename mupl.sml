use "util.sml";
Control.Print.printDepth := 50;
Control.Print.printLength := 100;

structure Mupl = struct

  structure Env = RedBlackMapFn (Util.Comp.String)

  datatype expr
    = Var of string
    | Int of int
    | Unit
    | Add of expr * expr
    | Fun of string option * string * expr
    | Closure of {context : expr Env.map
                 ,name    : string option
                 ,formal  : string
                 ,body    : expr}
    | IfGreater of {left : expr, right : expr, yes : expr, no : expr}
    | Let of string * expr * expr
    | Call of expr * expr
    | Pair of expr * expr
    | Fst of expr
    | Snd of expr
    | IsUnit of expr

  exception RuntimeError of string

  fun op <- (Var a, b) body = Let (a, b, body)
    | op <- _ _ = raise (RuntimeError "bind: left hand side must be a Var")

  fun op <-- (Var name, Fun (_, formal, fbody)) body = 
      Let (name, Fun (SOME name, formal, fbody), body) 
    | op <-- (Var a, b) body =
      Let (a, b, body)
    | op <-- _ _ = raise (RuntimeError "bindrec: left hand side must be a Var")

  fun op <: (Var name, Fun (_, formal, fbody)) =
      Fun (SOME name, formal, fbody)
    | op <: _ = raise (RuntimeError 
                       "fundec: left must be Var, right must be Fun")


  val op ++ = Add

  fun op :=> (Var formal, body) = Fun (NONE, formal, body)
    | op :=> _ = raise (RuntimeError "lambda: left hand side must be a Var")

  val op </ = Call

  val op <\ = Call

  fun op /> (x, f) = Call (f, x)

  val op % = Var

  fun eval e = let
      fun evalUnder env =
       fn value as (Int _ | Unit | Closure _) => value

        | Var name =>
          (case Env.find (env, name)
             of SOME e' => e'
              | NONE => raise (RuntimeError ("var: unbound name `" ^ 
                                             name ^ "`")))

        | Add (e1, e2) =>
          (case (evalUnder env e1, evalUnder env e2)
             of (Int a, Int b) => Int (a + b)
              | _ => raise (RuntimeError "add: adding non-ints"))

        | Fun (name, formal, body) =>
          Closure {context = env, name = name, formal = formal, body = body}

        | IfGreater {left, right, yes, no} =>
          (case (evalUnder env left, evalUnder env right)
             of (Int a, Int b) => evalUnder env (if a > b then yes else no)
              | _ => raise (RuntimeError "if-greater: comparing non-ints"))

        | Let (name, e', body) =>
          evalUnder (Env.insert (env, name, evalUnder env e')) body

        | Call (func, arg) =>
          (case (evalUnder env func, evalUnder env arg)
             of (c as Closure {context, name, formal, body}, actual) => let
                  val base = case name
                               of NONE => context
                                | SOME s => Env.insert (context, s, c)
                in
                  evalUnder (Env.insert (base, formal, actual)) body
                end
              | _ => raise (RuntimeError "call: calling a non-closure"))

        | Pair (e1, e2) => Pair (evalUnder env e1, evalUnder env e2)

        | Fst e' =>
          (case evalUnder env e'
             of Pair (a, _) => a
              | _ => raise (RuntimeError "fst: applying to non-pair"))

        | Snd e' =>
          (case evalUnder env e'
             of Pair (_, b) => b
              | _ => raise (RuntimeError "snd: applying to non-pair"))

        | IsUnit e' =>
          (case evalUnder env e'
             of Unit => Int 1
              | _ => Int 0)
    in
      evalUnder Env.empty e
    end

  fun fromList [] = Unit
    | fromList (x :: xs) = Pair (x, fromList xs)

  fun toList Unit = []
    | toList (Pair (x, xs)) = x :: toList xs
    | toList _ = raise (RuntimeError "toList: applying to non-pair")

  fun ifUnit e yes no = 
      IfGreater {left = IsUnit e, right = Int 0, yes = yes, no = no}

  fun ifEq (e1, e2) yes no =
      IfGreater {left = e1, right = e2, yes = no
                ,no = IfGreater {left = e2, right = e1, yes = no, no = yes}}

  fun cons h t = Pair (h, t)

  fun when e (yes, no) = ifEq (Int 0, e) no yes

  fun otherwise pair = pair

  val map = Fun (NONE, "f", Fun (SOME "loop", "xs"
                                ,ifUnit (Var "xs")
                                        Unit
                                        (cons (Call (Var "f", Fst (Var "xs")))
                                              (Call (Var "loop"
                                                    ,Snd (Var "xs"))))))

end

local
  infix 2 <-
  infix 2 <--
  infix 2 <:
  infix 9 ++
  infixr 3 :=>
  infix 4 \>
  infix 5 </
  infixr 6 <\
  infixr 1 $
  infixr 8 otherwise
  open Mupl
  val op $ = Util.$
in
  val e1 = eval $ Var "foo" <- Int 1 $ Add (Int 100, Var "foo")
  val xs = fromList [Int 1, Int 2, Int 3]
  val e2 = eval $ Fst o Snd o Snd $ xs
  val e3 = eval $ Var "a" <- Int 200 
                $ Var "b" <- Var "a" ++ Var "a" ++ Var "a" ++ Var "a" ++ Var "a"
                $ Var "c" <- Int 30 
                $ Var "a" ++ Var "b" ++ Var "c" ++ Int 4
  val dec = %"n" :=> %"n" ++ Int ~1
  val e4 = eval $ dec </ Int 100
  val e5 = eval $ %"inc" <- %"n" :=> %"n" ++ Int 1 
                $ dec </ %"inc" <\ Int 0
  val e6 = (toList o eval)
         $ %"map" <- map
         $ %"+n" <- %"n" :=> %"map" </ (%"x" :=> %"x" ++ %"n")
         $ %"+n" </ Int 100 </ xs

  val mul = %"*" <: %"n" :=> %"m" :=> 
              (when (%"m") $ (%"n" ++ (%"*" </ %"n" </ dec <\ %"m"))
                   otherwise (Int 0))
            
  val e7 = eval $ mul </ Int 64 </ Int 16
  val e8 = (toList o eval) $ map </ mul <\ e7 </ xs
end
