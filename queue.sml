signature STACK = sig
  type 'a stack
  exception Empty

  val empty : 'a stack
  val isEmpty : 'a stack -> bool
  val push : 'a stack -> 'a -> 'a stack
  val pop : 'a stack -> 'a stack
  val top : 'a stack -> 'a
  val map : ('a -> 'b) -> 'a stack -> 'b stack
  val forEach : ('a -> unit) -> 'a stack -> unit
  val fold : ('b -> 'a -> 'b) -> 'b -> 'a stack -> 'b
  val fromList : 'a list -> 'a stack
end

signature QUEUE = sig
  type 'a queue
  exception Empty

  val empty : 'a queue
  val isEmpty : 'a queue -> bool
  val enqueue : 'a queue -> 'a -> 'a queue
  val dequeue : 'a queue -> 'a queue
  val front : 'a queue -> 'a
  val map : ('a -> 'b) -> 'a queue -> 'b queue
  val forEach : ('a -> unit) -> 'a queue -> unit
  val fold : ('b -> 'a -> 'b) -> 'b -> 'a queue -> 'b
end

structure Stack :> STACK = struct
  type 'a stack = 'a list
  exception Empty = List.Empty

  val empty = []
  val isEmpty = null
  fun push xs x = x :: xs
  val pop = tl
  val top = hd
  val map = List.map
  val forEach = List.app
  fun fold f = List.foldl (fn (x, acc) => f acc x) 
  val fromList = rev
end

structure Queue :> QUEUE = struct
  structure S = Stack
  type 'a queue = 'a S.stack * 'a S.stack
  exception Empty

  val empty = (S.empty, S.empty)

  fun isEmpty (s1, s2) = S.isEmpty s1 andalso S.isEmpty s2

  fun enqueue (s1, s2) a = (S.push s1 a, s2)

  fun rev s = S.fold S.push S.empty s

  fun dequeue (s1, s2) =
    case (S.isEmpty s1, S.isEmpty s2)
      of (true, true) => raise Empty
       | (false, true) => (S.empty, S.pop (rev s1))
       | (_, false) => (s1, S.pop s2)

  fun front (s1, s2) =
    case (S.isEmpty s1, S.isEmpty s2)
      of (true, true) => raise Empty
       | (false, true) => S.top (rev s1)
       | (_, false) => S.top s2

  fun map f (s1, s2) = (S.map f s1, S.map f s2)

  fun forEach f (s1, s2) = (S.forEach f s2; S.forEach f (rev s1))
  
  fun fold f acc (s1, s2) =
    S.fold f acc (S.fold S.push s2 s1)
end
