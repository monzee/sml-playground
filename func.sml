signature DATASET = sig
  type data
  val numRows : data -> int
  val getRow : data -> int -> real list
end

functor NeighborFinder (Dataset : DATASET) :> sig
  val nearestDistance : Dataset.data -> real list -> real
end = struct
  fun nearestDistance data row = let
    fun minSquareDist (currentRow, acc) = let
      val squareDist = foldl (fn ((r, c), sum) =>
        sum + Math.pow (r - c, 2.0)) 0.0 (ListPair.zip (row, currentRow))
    in
      if squareDist < acc
      then squareDist
      else acc
    end
  in
    Math.sqrt (foldl minSquareDist Real.posInf 
      (List.tabulate (Dataset.numRows data, Dataset.getRow data)))
  end
end

structure VectorData = struct
  type data = real list vector
  val numRows = Vector.length
  fun getRow vec i = Vector.sub (vec, i)
end

structure RandomData :> sig
  datatype data = any | length of int | size of int * int
  val numRows : data -> int
  val getRow : data -> int -> real list
end = struct
  datatype data = any | length of int | size of int * int
  val d = Date.fromTimeLocal (Time.now ())
  val seed = Random.rand (Date.minute d, Date.second d)
  fun genAtMost max _ = max * Random.randReal seed
  val randomLength = Random.randRange (10, 50) seed

  fun numRows (size (_, v)) = v 
    | numRows _ = Random.randRange (200, 500) seed

  fun getRow any _ = List.tabulate (randomLength, genAtMost 100.0) 
    | getRow (length n) _ = List.tabulate (n, genAtMost 100.0) 
    | getRow (size (h, _)) _ = List.tabulate (h, genAtMost 100.0)
end

fun generate (cols, rows) =
  Vector.tabulate (rows, RandomData.getRow (RandomData.length cols))

val data = generate (5, 500)
structure VectorFinder = NeighborFinder (VectorData)
val shouldBeZero = VectorFinder.nearestDistance data (VectorData.getRow data 10)

structure RandomFinder = NeighborFinder (RandomData)
val meaningless = RandomFinder.nearestDistance (RandomData.length 5) [1.0, 2.0, 3.0, 4.0, 5.0]
