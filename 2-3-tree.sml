type 'a comparator = 'a * 'a -> order
datatype ('k, 'v) tree =
    Leaf of 'k comparator
|   Node2 of {key: 'k, value: 'v, comp: 'k comparator,
              left: ('k, 'v) tree, right: ('k, 'v) tree} 
|   Node3 of {keys: 'k * 'k, values: 'v * 'v, comp: 'k comparator,
              1: ('k, 'v) tree, 2: ('k, 'v) tree, 3: ('k, 'v) tree}

datatype ('k, 'v) fresh =
    Normal of ('k, 'v) tree
|   Degen of {keys: 'k * 'k * 'k, values: 'v * 'v * 'v,
              nodes: ('k, 'v) tree * ('k, 'v) tree *
                     ('k, 'v) tree * ('k, 'v) tree}

exception InvariantViolation

val rec get: ('k, 'v) tree -> 'k -> 'v option =
fn node => fn k =>
    case node
    of  Leaf _ => NONE
    |   Node2 {key, value, comp, left, right} =>
        (case comp (k, key)
        of  EQUAL => SOME value
        |   LESS => get left k
        |   GREATER => get right k)
    |   Node3 {keys = (k1, k2), values = (v1, v2), comp, 
               1 = left, 2 = mid, 3 = right} =>
        (case (comp (k, k1), comp (k, k2))
        of  (EQUAL, _) => SOME v1
        |   (_, EQUAL) => SOME v2
        |   (GREATER, LESS) => get mid k
        |   (LESS, LESS) => get left k
        |   (GREATER, GREATER) => get right k
        |   _ => raise InvariantViolation)
        

local
in
    val rec put: ('k, 'v) tree -> 'k * 'v -> ('k, 'v) tree =
    fn node => fn (k, v) =>
        case node
        of  Leaf c => Node2 {key = k, value = v, comp = c,
                             left = node, right = node}
        |   Node2 {key, value, comp, left, right} =>
            (case comp (k, key)
            of  EQUAL => Node2 {key = key, value = v, comp = comp,
                                left = left, right = right}
            |   LESS => 
                (case tryput left (k, v)
                of  Normal node => Node2 {key = key, value = v, comp = comp,
                                          left = node, right = right}
                |   Degen {keys = (k1, k2, k3), values = (v1, v2, v3), 
                           nodes = (n1, n2, n3, n4)} =>
                    Node3 {keys = (k, key), values = (v, value), comp = comp,
                           1 = ???,  2 = ???, 3 = right})
            |   GREATER => let
            in
            end)
end
