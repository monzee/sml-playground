signature BOARD = sig
  type t
  type slot
  val BLOCK_SIZE : int
  val make : int list list -> t
  val at : slot -> t -> int
  val row : slot -> t -> int list
  val col : slot -> t -> int list
  val block : slot -> t -> int list
  val rows : t -> int list list
  val cols : t -> int list list
  val blocks : t -> int list list
  val update : slot -> int -> t -> t
  val findOpen : t -> slot option
  val foldr : (int * int -> int -> 'a -> 'a) -> 'a -> t -> 'a
end

functor SudokuFn (Board : BOARD) : sig
  val board : int list list -> Board.t
  val solve : Board.t -> Board.t option
  val solveAll : Board.t -> Board.t list
  val show : Board.t -> string
end = struct

  exception Unimplemented

  val VALID = 
      IntBinarySet.fromList (List.tabulate (Board.BLOCK_SIZE * Board.BLOCK_SIZE, 
                                            fn n => n + 1))

  val board = Board.make

  fun toSet xs = foldl IntBinarySet.add' IntBinarySet.empty xs

  fun validValues xs = IntBinarySet.equal (VALID, toSet xs)

  fun isValid b =
      List.all (fn f => List.all validValues (f b))
               [Board.rows, Board.cols, Board.blocks]

  fun branch slot b = let
      val taken = List.concat [Board.row slot b, Board.col slot b, 
                               Board.block slot b]
      val available = foldl IntBinarySet.subtract' VALID taken
    in
      IntBinarySet.foldl (fn (n, xs) => Board.update slot n b :: xs) [] available
    end

  fun solve b = 
      case Board.findOpen b
        of NONE => if isValid b then SOME b else NONE
         | SOME slot => try (branch slot b)
  and try [] = NONE
    | try (b :: bs) =
      case solve b
        of NONE => try bs
         | solution => solution
          
  fun solveAll b =
      case Board.findOpen b
        of NONE => if isValid b then [b] else []
         | SOME slot => List.concat (map solveAll (branch slot b))

  local
    val m = Board.BLOCK_SIZE * Board.BLOCK_SIZE

    val hdiv = let
        fun recur s 1 = "\n ─" ^ s
          | recur s n = 
            if n <> m andalso n mod Board.BLOCK_SIZE = 0 then
              recur ("───┼" ^ s) (n - 1)
            else
              recur ("──" ^ s) (n - 1)
      in
        recur "" m
      end

    fun stringify (row, col) n out =
        (if row <> 0 andalso col = 0 andalso row mod Board.BLOCK_SIZE = 0 then hdiv else "") ^
        (if col <> 0 andalso col mod Board.BLOCK_SIZE = 0 then " │" else "") ^
        (if col mod m = 0 then "\n " else " ") ^
        (if n <> 0 then Int.toString n else "_") ^
        out
  in
    fun show b =
        Board.foldr stringify "\n" b
  end
end

functor VecBoard (val BLOCK_SIZE : int) = struct
  type t = int vector vector
  type slot = int * int

  val BLOCK_SIZE = BLOCK_SIZE
  val LENGTH = BLOCK_SIZE * BLOCK_SIZE
  val OFFSETS = let
      val xs = List.tabulate (BLOCK_SIZE, fn n => n)
    in
      List.concat (map (fn x => map (fn y => (x, y)) xs) xs)
    end
  val EMPTY = 0
  
  exception WrongDimensions

  fun make xss = 
      if length xss <> LENGTH orelse 
         List.exists (fn xs => length xs <> LENGTH) xss
      then raise WrongDimensions
      else Vector.fromList (map Vector.fromList xss)

  fun at (row, col) xss = Vector.sub (Vector.sub (xss, row), col)

  fun toList v = Vector.foldr (op ::) [] v

  fun row (n, _) xss = toList (Vector.sub (xss, n))

  fun col (_, n) xss = toList (Vector.map (fn xs => Vector.sub (xs, n)) xss) 

  fun block (row, col) xss = let
      val r = BLOCK_SIZE * (row div BLOCK_SIZE)
      val c = BLOCK_SIZE * (col div BLOCK_SIZE)
    in
      map (fn (dr, dc) => at (r + dr, c + dc) xss) OFFSETS
    end

  fun rows xss = toList (Vector.map toList xss)

  fun cols xss = List.tabulate (LENGTH, fn n => col (0, n) xss)

  fun blocks xss = 
      map (fn (r, c) => block (BLOCK_SIZE * r, BLOCK_SIZE * c) xss) OFFSETS

  fun update (row, col) n xss = let
      val xs = Vector.sub (xss, row)
    in
      Vector.update (xss, row, Vector.update (xs, col, n))
    end

  fun findOpen xss = let
      fun recur row col =
          if row = LENGTH then
            NONE
          else if col = LENGTH then
            recur (row + 1) 0
          else if at (row, col) xss = EMPTY then
            SOME (row, col)
          else
            recur row (col + 1)
    in
      recur 0 0
    end

  fun foldr f acc b = let
      val n = LENGTH - 1
      fun recur acc ~1 _ = acc
        | recur acc row ~1 = recur acc (row - 1) n
        | recur acc row col = 
          recur (f (row, col) (at (row, col) b) acc) row (col - 1)
    in
      recur acc n n
    end
end

structure Test = SudokuFn (VecBoard (val BLOCK_SIZE = 2))
structure Sudoku = SudokuFn (VecBoard (val BLOCK_SIZE = 3))
val board = Sudoku.board [[5, 3, 0, 0, 7, 0, 0, 0, 0]
                         ,[6, 0, 0, 1, 9, 5, 0, 0, 0]
                         ,[0, 9, 8, 0, 0, 0, 0, 6, 0]
                         ,[8, 0, 0, 0, 6, 0, 0, 0, 3]
                         ,[4, 0, 0, 8, 0, 3, 0, 0, 1]
                         ,[7, 0, 0, 0, 2, 0, 0, 0, 6]
                         ,[0, 6, 0, 0, 0, 0, 2, 8, 0]
                         ,[0, 0, 0, 4, 1, 9, 0, 0, 5]
                         ,[0, 0, 0, 0, 8, 0, 0, 7, 9]]

val mult = Sudoku.board [[0,8,0, 0,0,9, 7,4,3]
                        ,[0,5,0, 0,0,8, 0,1,0]
                        ,[0,1,0, 0,0,0, 0,0,0]

                        ,[8,0,0, 9,0,5, 0,0,0]
                        ,[0,0,0, 8,0,4, 0,0,0]
                        ,[0,0,0, 3,0,0, 0,0,6]

                        ,[0,0,0, 0,0,0, 0,7,0]
                        ,[0,3,0, 5,0,0, 0,8,0]
                        ,[9,7,2, 4,0,0, 0,5,0]]

;(print o Sudoku.show o valOf o Sudoku.solve) board
