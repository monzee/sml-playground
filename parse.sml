functor ParseFn (type input) : sig
  type 'a parser
  type 'a result

  val return : 'a -> 'a parser
  val >>= : 'a parser * ('a -> 'b parser) -> 'b parser
  val >>  : 'a parser * 'b parser -> 'b parser
  val ||  : 'a parser * 'a parser -> 'a parser
  val failure : 'a parser
  val accept : (input -> 'a option) -> 'a parser
  val when : (input -> bool) -> input parser
  val any : input parser
  val optional : 'a parser -> 'a list parser
  val many : 'a parser -> 'a list parser
  val eof : unit parser
end = struct
  type 'a result = ('a * input list) option
  type 'a parser = input list -> 'a result

  infix 1 ||
  infixr 0 >>=
  infixr 0 >>

  fun return x i = SOME (x, i)

  fun (p >>= q) i =
      Option.mapPartial (fn (res, i') => q res i') (p i)

  fun p >> q = p >>= (fn _ => q)

  fun (p || q) i =
      case p i
        of NONE => q i
         | r => r

  fun failure _ = NONE

  fun accept _ [] = NONE
    | accept p (x :: xs) = Option.map (fn x' => (x', xs)) (p x)

  fun when p = accept (fn i => if p i then SOME i else NONE)

  fun any i = accept SOME i

  fun optional p = many p || return []
  and many p = p >>= (fn x =>
               optional p >>= (fn xs =>
               return (x :: xs)))

  fun eof [] = SOME ((), [])
    | eof _ = NONE
end

