structure Stream :> sig
  type 'a stream

  val seq : 'a -> ('a -> 'a) -> 'a stream
  val make : 'a -> ('a -> 'b * 'a) -> 'b stream
  val const : 'a -> 'a stream
  val finite : 'a -> ('a -> ('b * 'a) option) -> 'b stream
  val get : 'a stream -> 'a
  val maybeGet : 'a stream -> 'a option
  val next : 'a stream -> 'a stream
  val take : int -> 'a stream -> 'a list
  val maybeTake : int -> 'a stream -> 'a option list
  val drop : int -> 'a stream -> 'a stream
  val filter : ('a -> bool) -> 'a stream -> 'a stream
  val limit : int -> 'a stream -> 'a stream
  val zip : 'a stream -> 'b stream -> ('a * 'b) stream
  val map : ('a -> 'b) -> 'a stream -> 'b stream
  val chunk : int -> 'a stream -> 'a list stream
end = struct
  datatype 'a stream = Stream of 'a computation ref
       and 'a computation = Future of unit -> 'a * 'a stream 
                          | Computed of 'a * 'a stream
  exception Exhausted
  
  fun get (Stream s) = 
      case !s 
        of Computed (c, _) => c
         | Future f => let
              val (c, n) = f ()
            in
              s := Computed (c, n);
              c
            end

  fun next (Stream s) =
      case !s
        of Computed (_, n) => n
         | Future f => let
              val (c, n) = f ()
            in
              s := Computed (c, n);
              n
            end

  fun maybeGet stream = SOME (get stream) handle Exhausted => NONE

  fun make a gen = 
      Stream (ref (Future (fn () => let val (e, a') = gen a 
                                     in (e, make a' gen) end)))

  fun seq a gen = make a (fn (a) => (a, gen a))

  fun const e = make () (fn _ => (e, ()))

  fun finite a gen =
      Stream (ref (Future (fn () => case gen a
                                      of NONE => raise Exhausted
                                       | SOME (e, a') => (e, finite a' gen))))

  fun take 0 _ = []
    | take n stream = get stream :: take (n - 1) (next stream)

  fun maybeTake 0 _ = []
    | maybeTake n stream = 
      maybeGet stream :: maybeTake (n - 1) 
                                   (next stream handle Exhausted => stream)

  fun drop 0 stream = stream
    | drop n stream = drop (n - 1) (next stream)

  fun filter pred stream = let
      fun satisfactory s = let
          val e = get s
        in
          if pred e
          then (e, next s)
          else satisfactory (next s)
        end
    in
      make stream satisfactory
    end

  fun limit n stream = 
      finite (stream, 0) (fn (s, i) => if i < n
                                       then SOME (get s, (next s, i + 1))
                                       else NONE)

  fun zip s1 s2 =
      make (s1, s2) (fn (s, t) => ((get s, get t), (next s, next t)))
      
  fun map f stream = make stream (fn s => (f (get s), next s))

  fun chunk size stream = let
      fun loop (0, acc) s = (rev acc, s)
        | loop (n, acc) s = loop (n - 1, get s :: acc) (next s)
    in
      make stream (loop (size, []))
    end

end

val nats = Stream.seq 1 (fn n => n + 1)
val evens = Stream.filter (fn n => n mod 2 = 0) nats
val funnyNumbers = Stream.make 1 (fn n => (if n mod 5 = 0 then ~n else n, n + 1))

val danThenDog = Stream.make true 
                   (fn human => (if human then "dan.jpg" else "dog.jpg", 
                                 not human))

fun addZero s = Stream.make s (fn s => ((0, Stream.get s), Stream.next s))

val twentyDogs = Stream.limit 20 (Stream.filter (fn s => s <> "dan.jpg") 
                                                danThenDog)

