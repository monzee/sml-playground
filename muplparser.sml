use "parse.sml";
Control.Print.printLength := 100;
Control.Print.printDepth := 50;

exception Unimplemented
exception ParseError

structure Token : sig
  datatype token 
    = NUM of int
    | NAME of string
    | KEYWORD of string
    | UNIT
    | DECL
    | GT
    | ARROW
    | CONS
    | LPAREN
    | RPAREN
    | PLUS
    | SEMI
    | S
    | FAIL of char

  val lex : string -> token list
end = struct

  datatype token 
    = NUM of int
    | NAME of string
    | KEYWORD of string
    | UNIT
    | DECL
    | GT
    | ARROW
    | CONS
    | LPAREN
    | RPAREN
    | PLUS
    | SEMI
    | S
    | FAIL of char

  structure CharParser = ParseFn (type input = char)
  open CharParser

  infixr 1 >>=
  infixr 1 >>
  infix 2 ||
  infix 0 $ fun f $ x = f x

  val digit = when Char.isDigit
  val alpha = when Char.isAlpha
  val space = when Char.isSpace >> return ()
  fun eq ch = when (fn c => case Char.compare (c, ch)
                              of EQUAL => true
                               | _ => false)
  
  val natural = many digit >>= return o valOf o Int.fromString o String.implode
  val number = natural || (eq #"-" >> natural >>= return o ~)

  fun str s = let
      fun chars [] = return []
        | chars (x :: xs) =
          eq x >>
          chars xs >>
          return (x :: xs)
    in
      chars (String.explode s) >>=
      return o String.implode
    end

  fun tok s = optional space >>
              str s >>= (fn s' =>
              optional space >>
              return s')
  
  fun word s = optional space >>
               str s >>= (fn s' =>
               space || eof >>
               return s')

  val tokens = (tok "()" >> return UNIT)
            || (tok "->" >> return ARROW)
            || (tok "::" >> return CONS)
            || (tok ":" >> return DECL)
            || (tok ">" >> return GT)
            || (tok "(" >> return LPAREN)
            || (tok ")" >> return RPAREN)
            || (tok "+" >> return PLUS)
            || (tok ";" >> return SEMI)
            || (word "let" || word "in" || word "fn" || 
                word "if" || word "then" || word "else" ||
                word "hd" || word "tl" || tok "unit?" >>= 
                return o KEYWORD)
            || (number >>= return o NUM)
            || (optional space >>
                eq #"_" || alpha >>= (fn first =>
                optional (eq #"_" || alpha || digit) >>= (fn rest =>
                return o NAME o String.implode $ first :: rest)))
            || (any >>= return o FAIL)
            
  fun lex s = 
      case (optional tokens) (String.explode s)
        of SOME (xs, []) => S :: xs
         | _ => raise ParseError
end


structure Mupl : sig
  type expr
  val parse : Token.token list -> (string * expr) list * expr
end = struct
  (*
    VarDec := NAME DECL Expr SEMI
    FunDec := NAME NAME DECL Expr SEMI
    Fun := `fn` NAME ARROW Expr
    Cond := `if` Expr GT Expr `then` Expr `else` Expr
    Let := `let` NAME DECL Expr `in` Expr
    Fst := `hd` Expr
    Snd := `tl` Expr
    IsUnit := `unit?` Expr
    Expr := (NUM | NAME | UNIT | Fun | Cond | Let | Fst | Snd | IsUnit | LPAREN Expr RPAREN) Expr'
    Expr' := Call' | Add' | Pair' | e
    Add' := PLUS Expr
    Call' := Expr
    Pair' := CONS Expr
    Decl := VarDec | FunDec
    Prog := S Decl* Expr
   *)

  structure TokenParser = ParseFn (type input = Token.token)
  open TokenParser
  infixr 1 >>=
  infixr 1 >>
  infix 2 ||
  infix 0 $ fun f $ x = f x

  datatype expr 
    = Lit of int 
    | Name of string 
    | Func of string * expr
    | Unit
    | IfGreater of expr * expr * expr * expr
    | Let of string * expr * expr
    | Fst of expr
    | Snd of expr
    | IsUnit of expr
    | Call of expr * expr
    | Add of expr * expr
    | Pair of expr * expr

  fun eq token = when (fn t => t = token)

  fun expr i = (number || name || unit_ || func || cond || let_ || fst || snd ||
                isUnit || (eq Token.LPAREN >>
                           expr >>= (fn e =>
                           eq Token.RPAREN >>
                           return e))) >>=
                expr' $ i

  and name i = accept (fn Token.NAME s => SOME (Name s) | _ => NONE) i

  and number i = accept (fn Token.NUM n => SOME (Lit n) | _ => NONE) i

  and unit_ i = eq Token.UNIT >> return Unit $ i

  and func i = eq (Token.KEYWORD "fn") >>
               name >>= (fn Name p =>
               eq Token.ARROW >>
               expr >>= (fn e =>
               return (Func (p, e)))) $ i
  
  and cond i = eq (Token.KEYWORD "if") >>
               expr >>= (fn left =>
               eq Token.GT >>
               expr >>= (fn right =>
               eq (Token.KEYWORD "then") >>
               expr >>= (fn yes =>
               eq (Token.KEYWORD "else") >>
               expr >>= (fn no =>
               return (IfGreater (left, right, yes, no)))))) $ i

  and let_ i = eq (Token.KEYWORD "let") >>
               name >>= (fn Name name =>
               eq Token.DECL >>
               expr >>= (fn value =>
               eq (Token.KEYWORD "in") >>
               expr >>= (fn body =>
               return (Let (name, value, body))))) $ i

  and fst i = eq (Token.KEYWORD "hd") >>
              expr >>= 
              return o Fst $ i

  and snd i = eq (Token.KEYWORD "tl") >>
              expr >>=
              return o Snd $ i

  and isUnit i = eq (Token.KEYWORD "unit?") >>
                 expr >>=
                 return o IsUnit $ i

  and expr' left = call' left || add' left || pair' left || return left
                      
  (* should make ths left assoc *)
  and call' left = expr >>= (fn right => return (Call (left, right)))

  (* this too *)
  and add' left = eq Token.PLUS >>
                  expr >>= (fn right =>
                  return (Add (left, right)))

  and pair' left = eq Token.CONS >>
                   expr >>= (fn right =>
                   return (Pair (left, right)))

  val varDec = name >>= (fn Name var =>
               eq Token.DECL >>
               expr >>= (fn value =>
               eq Token.SEMI >>
               return (var, value)))

  val funDec = name >>= (fn Name func =>
               name >>= (fn Name param => 
               eq Token.DECL >>
               expr >>= (fn body => 
               eq Token.SEMI >>
               return (func, Func (param, body)))))

  val decl = varDec || funDec

  val prog = eq Token.S >>
             optional decl >>= (fn bindings =>
             expr >>= (fn e =>
             optional (eq Token.SEMI) >>
             return (bindings, e)))
  
  fun parse tokens = 
      case prog tokens
        of SOME ((bindings, expr), []) => (bindings, expr)
         | _ => raise ParseError

end


fun parse s = Mupl.parse (Token.lex s)

val e0 = parse "one : 1; one"
val e1 = parse "two x : 2; two"
val e2 = parse "foo x : (if x > 0 then 1 else ((0))); foo"
val e3 = parse "let e : unit? xs in if e > 0 then foo else bar"
val e4 = parse "a b::c"
val e5 = parse ("map f : fn xs -> if unit? xs > 0 then () else f (fst xs) :: map f (snd xs);"
               ^"map (fn x -> x + 1) (1 :: 2 :: 3 :: 4 :: ())")
