use "util.sml";
infix $
infix |>
val op $ = Util.$
val op |> = Util.|>
val flip = Util.flip
val untuple = Util.untuple2
val uncurry = Util.uncurry2

signature TABLE = sig
  type key
  type 'a table
  val empty : 'a table
  val get : key -> 'a table -> 'a option
  val has : key -> 'a table -> bool
  val put : key * 'a -> 'a table -> 'a table
  val fromList : (key * 'a) list -> 'a table
  val toList : 'a table -> (key * 'a) list
  val del : key -> 'a table -> 'a table
  val map : (key * 'a -> 'a) -> 'a table -> 'a table
  val fold : ('a -> key * 'b -> 'a) -> 'a -> 'b table -> 'a
  val floor : key -> 'a table -> key
  val ceil : key -> 'a table -> key
end

functor MakeBstMap (Key : ORD_KEY) :> TABLE where type key = Key.ord_key = struct
  type key = Key.ord_key
  datatype 'a table = Leaf | Node of {key : key, value : 'a,
                                      left : 'a table, right: 'a table}
  
  val empty = Leaf

  fun get _ Leaf = NONE
    | get k (Node {key, value, left, right}) =
      case Key.compare (k, key)
        of EQUAL   => SOME value
         | LESS    => get k left
         | GREATER => get k right

  fun has k = isSome o (get k)

  fun put (k, v) Leaf = Node {key = k, value = v, left = Leaf, right = Leaf}
    | put (data as (k, v)) (Node {key, value, left, right}) =
      case Key.compare (k, key)
        of EQUAL   => Node {key = k, value = v, left = left, right = right}
         | LESS    => Node {key = key, value = value, 
                            left = put data left, right = right}
         | GREATER => Node {key = key, value = value,
                            left = left, right = put data right}

  fun fromList xs = foldl (uncurry put) Leaf xs

  fun toList Leaf = []
    | toList (Node {key, value, left, right}) = 
      toList left @ [(key, value)] @ toList right

  fun takeSmallest Leaf = NONE
    | takeSmallest (Node {key, value, left, right}) =
      case takeSmallest left
        of NONE => SOME (key, value, right)
         | SOME (k, v, t) => SOME (k, v, Node {key = key, value = value,
                                               left = t, right = right})

  fun del _ Leaf = Leaf
    | del k (Node {key, value, left, right}) =
      case Key.compare (k, key)
        of LESS    => Node {key = key, value = value, 
                            left = del k left, right = right}
         | GREATER => Node {key = key, value = value,
                            left = left, right = del k right}
         | EQUAL   => case takeSmallest right
                        of NONE => left
                         | SOME (k', v', r') => Node {key = k', value = v', 
                                                      left = left, right = r'}

  fun map _ Leaf = Leaf
    | map f (Node {key, value, left, right}) = 
      Node {key = key, value = f (key, value), 
            left = map f left, right = map f right}

  fun fold _ zero Leaf = zero
    | fold f a (Node {key, value, left, right}) =
      fold f (f (fold f a left) (key, value)) right

  fun floor k Leaf = k
    | floor k (Node {key, value, left, right}) =
      case Key.compare (k, key)
        of LESS    => floor k left
         | GREATER => key
         | EQUAL   => k

  fun ceil k Leaf = k
    | ceil k (Node {key, value, left, right}) =
      case Key.compare (k, key)
        of LESS    => key
         | GREATER => ceil k right
         | EQUAL   => k
end

structure IntMap = MakeBstMap (Util.Comp.Int)
structure Dict = MakeBstMap (Util.Comp.String)
structure DictI = MakeBstMap (Util.Comp.StringI)

