structure Parse : sig
  type 'a parser
  type 'a result

  val return : 'a -> 'a parser
  val parse : 'a parser -> string -> 'a result
  val || : 'a parser * 'a parser -> 'a parser
  val >>= : 'a parser * ('a -> 'b parser) -> 'b parser
  val >> : 'a parser * 'b parser -> 'b parser

  val failure : 'a parser
  val any : char parser
  val digit : char parser
  val letter : char parser
  val alnum : char parser
  val symbol : char -> char parser
  val str : string -> string parser
  val optional : 'a parser -> 'a list parser
  val many : 'a parser -> 'a list parser
  val nat : int parser
  val number : int parser
  val space : unit parser
end = struct
  type 'a result = ('a * char list) option
  type 'a parser = char list -> 'a result

  fun failure _ = NONE

  fun accept _ [] = NONE
    | accept p (x :: xs) = if p x then SOME (x, xs) else NONE

  fun any s = accept (fn _ => true) s

  infix 1 ||
  fun op || (p, q) s =
      case p s
        of NONE => q s
         | r    => r

  infixr 0 >>=
  fun op >>= (p, q) s =
      Option.mapPartial (fn (res, s') => q res s') (p s)

  infixr 0 >>
  fun op >> (p, q) = p >>= (fn _ => q)

  fun return x s = SOME (x, s)

  fun optional p = many p || return []
  and many p = p >>= (fn x =>
               optional p >>= (fn xs =>
               return (x :: xs)))
    
  val digit = accept Char.isDigit

  val letter = accept Char.isAlpha

  val alnum = digit || letter

  fun symbol x = accept (fn c => case Char.compare (c, x)
                                   of EQUAL => true
                                    | _ => false)

  val nat = many digit >>= 
            return o valOf o Int.fromString o String.implode

  val number = nat || (symbol #"-" >>
                       nat >>=
                       return o ~)

  val space = accept Char.isSpace >>
              return ()

  fun str s = let
      fun chars [] = return []
        | chars (x :: xs) =
          symbol x >>
          chars xs >>
          return (x :: xs)
    in
      chars (String.explode s) >>= 
      return o String.implode
    end

  fun parse p = p o String.explode

end


local
  open Parse
  infixr 0 $   fun f $ x = f x
  infix  2 ||
  infixr 1 >>=
  infixr 1 >>
  datatype expr = Bind of string * expr | Int of int

  val name = (symbol #"_" || letter) >>= (fn x =>
             optional alnum >>= (fn xs =>
             return (String.implode (x :: xs))))

  fun keyword s = optional space >>
                  str s >>= (fn s' =>
                  many space >>
                  return s')

  fun token s = optional space >>
                str s >>= (fn s' =>
                optional space >>
                return s')
in
  val e1 = parse (failure || return #"x") "abc"
  val e2 = parse any "abc"
  val e3 = parse digit "123"
  val e4 = parse (digit || letter) "abc"
  val e5 = parse (str "abc") "abcde"
  val e6 = parse nat "-123abcde"
  val e7 = parse number "-1048576"
  val e8 = parse number "1024"
  val e9 = parse number "-002abc"
  val e10 = parse (keyword "val" >>
                   name >>= (fn identifier => 
                   token "=" >>
                   number >>= (fn n =>
                   optional (token ";") >>
                   return (Bind (identifier, Int n))))) "val  foo = 123   ;"
end
