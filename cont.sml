fun sum1 xs =
  case xs 
    of [] => 0
     | x :: xs' => x + sum1 xs'


fun sum2 xs = let
  fun aux ys acc =
    case ys
      of [] => acc
       | y :: ys' => aux ys' (y + acc)
in
  aux xs 0
end


fun sum3 xs = let
  fun aux [] k = k 0
    | aux (x :: xs') k = aux xs' (fn a => k (x + a))
in
  aux xs (fn x => x)
end


fun foldr f items acc =
  case items
    of [] => acc
     | x :: xs => f x (foldr f xs acc)

fun foldr2 f items acc = let
  fun aux [] k = k acc
    | aux (x :: xs) k = aux xs (fn a => k (f x a))
in
  aux items (fn x => x)
end


fun even x = x mod 2 = 0

fun txp1 x =
  if x = 1 then [1]
  else if even x then x :: txp1 (x div 2)
  else x :: txp1 (3 * x + 1)

fun txp2 x = let
  fun aux 1 k = k [1]
    | aux n k =
      if even n
      then aux (n div 2) (fn a => k (n :: a))
      else aux (3 * n + 1) (fn a => k (n :: a))
in
  aux x (fn x => x)
end

val callcc = SMLofNJ.Cont.callcc
val throw = SMLofNJ.Cont.throw

fun txp3 x = let
  fun aux 1 k = throw k [1]
    | aux n _ =
      if even n
      then n :: callcc (aux (n div 2))
      else n :: callcc (aux (3 * n + 1))
in
  callcc (aux x)
end

fun map f xs = let
  fun aux [] k = k []
    | aux (x :: xs') k = aux xs' (fn a => k (f x :: a))
in
  aux xs (fn x => x)
end

fun map' f xs = let
  fun aux [] k = throw k []
    | aux (x :: xs') _ = f x :: callcc (aux xs')
in
  callcc (aux xs)
end
