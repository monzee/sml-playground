use "util.sml";
infix |>
val op |> = Util.|>

(**
 * see http://www.4clojure.com/problem/53 
 *)

functor Subseq (E : ORD_KEY) : sig
  val longestIncreasing : E.ord_key list -> E.ord_key list
end = struct
  fun loop (x, (prev, probe, longest)) = let
      val p = case E.compare (x, prev) 
                of GREATER => x :: probe 
                 | _ => [x]
    in
      (x, p, if length p > length longest then p else longest)
    end

  fun atLeast n (_, _, longest) =
      if length longest >= n
      then rev longest
      else []

  fun longestIncreasing [] = []
    | longestIncreasing (x :: xs) = foldl loop (x, [x], [x]) xs |> atLeast 3
end

structure IntSubseq = Subseq (Util.Comp.Int)
structure RealSubseq = Subseq (Util.Comp.Real)
structure StringSubseq = Subseq (Util.Comp.StringI)

